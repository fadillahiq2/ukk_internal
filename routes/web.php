<?php

use App\Http\Controllers\SiswaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/siswa', [SiswaController::class, 'index'])->middleware('auth')->name('siswa.index');
Route::get('/', [SiswaController::class, 'create'])->name('siswa.create');
Route::get('/siswa/print/{id}', [SiswaController::class, 'print'])->name('siswa.print');
Route::post('/siswa/Post', [SiswaController::class, 'store'])->name('siswa.store');
Route::get('/siswa/edit/{id}', [SiswaController::class, 'edit'])->middleware('auth')->name('siswa.edit');
Route::put('/siswa/Update/{id}', [SiswaController::class, 'update'])->middleware('auth')->name('siswa.update');
Route::delete('/siswa/Delete/{id}', [SiswaController::class, 'destroy'])->middleware('auth')->name('siswa.destroy');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
