@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('PPDB SMK Merdeka Belajar') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ $message }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ route('siswa.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="nama">Nama</label>
                                <input type="text" maxlength="50" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama Anda" aria-describedby="helpId" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="jk">Jenis Kelamin</label>
                                <select class="form-control" name="jk" id="jk" required>
                                    <option value="" selected disabled>Pilih Jenis Kelamin</option>
                                    <option value="L">Laki-Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="alamat">Alamat</label>
                               <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="10" placeholder="Masukkan Alamat Anda" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="agama">Agama</label>
                                <select class="form-control" name="agama" id="agama" required>
                                    <option value="" selected disabled>Pilih Jenis Agama</option>
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Buddha">Buddha</option>
                                    <option value="Katholik">Katholik</option>
                                    <option value="Hindu">Hindu</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="asal_sekolah">Asal Sekolah</label>
                                <input type="text" name="asal_sekolah" id="asal_sekolah" class="form-control" placeholder="Masukkan Asal Sekolah Anda" aria-describedby="helpId" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="minat_jurusan">Minat Jurusan</label>
                                <select class="form-control" name="minat_jurusan" id="minat_jurusan" required>
                                    <option value="" selected disabled>Pilih Minat Jurusan</option>
                                    <option value="RPL">Rekayasa Perangkat Lunak</option>
                                    <option value="TBG">Tata Boga</option>
                                    <option value="TBS">Tata Busana</option>
                                    <option value="MMD">Multimedia</option>
                                </select>
                            </div>
                        </div>


                        <button class="btn btn-success btn-sm" type="submit">Simpan</button>
                        <button class="btn btn-warning btn-sm" type="reset">Reset</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
