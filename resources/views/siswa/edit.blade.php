@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('PPDB SMK Merdeka Belajar') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ $message }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ route('siswa.update', $siswa->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="nama">Nama</label>
                                <input type="text" maxlength="50" name="nama" id="nama" class="form-control" value="{{ $siswa->nama }}" placeholder="Masukkan Nama Anda" aria-describedby="helpId" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="jk">Jenis Kelamin</label>
                                <select class="form-control" value="{{ $siswa->jk }}" name="jk" id="jk" required>
                                    <option value="L" @if($siswa->jk == "L") selected='selected' @endif>Laki-Laki</option>
                                    <option value="P" @if($siswa->jk == "P") selected='selected' @endif>Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="alamat">Alamat</label>
                               <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="10" placeholder="Masukkan Alamat Anda" required>{{ $siswa->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="agama">Agama</label>
                                <select class="form-control" name="agama" value="{{ $siswa->agama }}" id="agama" required>
                                    <option value="Islam" @if($siswa->agama == "Islam") selected='selected' @endif>Islam</option>
                                    <option value="Kristen" @if($siswa->agama == "Kristen") selected='selected' @endif>Kristen</option>
                                    <option value="Buddha" @if($siswa->agama == "Buddha") selected='selected' @endif>Buddha</option>
                                    <option value="Katholik" @if($siswa->agama == "Katholik") selected='selected' @endif>Katholik</option>
                                    <option value="Hindu" @if($siswa->agama == "Hindu") selected='selected' @endif>Hindu</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="asal_sekolah">Asal Sekolah</label>
                                <input type="text" name="asal_sekolah" id="asal_sekolah" class="form-control" value="{{ $siswa->asal_sekolah }}" placeholder="Masukkan Asal Sekolah Anda" aria-describedby="helpId" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="minat_jurusan">Minat Jurusan</label>
                                <select class="form-control" name="minat_jurusan" value="{{ $siswa->minat_jurusan }}" id="minat_jurusan" required>
                                    <option value="RPL" @if($siswa->minat_jurusan == "RPL") selected='selected' @endif>Rekayasa Perangkat Lunak</option>
                                    <option value="TBG" @if($siswa->minat_jurusan == "TBG") selected='selected' @endif>Tata Boga</option>
                                    <option value="TBS" @if($siswa->minat_jurusan == "TBS") selected='selected' @endif>Tata Busana</option>
                                    <option value="MMD" @if($siswa->minat_jurusan == "MMD") selected='selected' @endif>Multimedia</option>
                                </select>
                            </div>
                        </div>


                        <button class="btn btn-warning btn-sm" type="submit">Perbarui</button>
                        <a class="btn btn-primary btn-sm" href="{{ url()->previous() }}">Kembali</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
