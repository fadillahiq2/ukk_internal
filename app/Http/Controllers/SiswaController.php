<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
class SiswaController extends Controller
{
    public function index()
    {
        Paginator::useBootstrap();
        $siswas = Siswa::latest()->paginate(10);

        return view('siswa.index', compact('siswas'))->with('i');
    }


    public function create()
    {
        return view('siswa.create');
    }

    public function print($id)
    {
        $siswa = Siswa::findOrFail($id);

        return view('siswa.print', compact('siswa'))->with('i');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|max:50',
            'jk' => 'required',
            'alamat' => 'required',
            'agama' => 'required',
            'asal_sekolah' => 'required',
            'minat_jurusan' => 'required'
        ]);

        $siswa = Siswa::create($request->all());

        return redirect()->route('siswa.print', $siswa->id)->with('success', 'Selamat, Anda sudah terdaftar di SMK Merdeka Belajar');
    }

    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);

        return view('siswa.edit', compact('siswa'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required|max:50',
            'jk' => 'required',
            'alamat' => 'required',
            'agama' => 'required|max:20',
            'asal_sekolah' => 'required',
            'minat_jurusan' => 'required|max:10'
        ]);

        $siswa = Siswa::findOrFail($id);
        $siswa->update($request->all());

        return redirect()->route('siswa.index')->with('success', 'Data berhasil diubah!');
    }

    public function destroy($id)
    {
        $siswa = Siswa::findOrFail($id);
        $siswa->delete();

        return redirect()->route('siswa.index')->with('success', 'Data sudah dihapus');
    }
}
